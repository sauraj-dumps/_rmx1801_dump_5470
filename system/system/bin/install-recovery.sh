#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:35833112:23fd70a6b1bf0ac1fc509b3e2ddf6098bea2fa92; then
  applypatch  EMMC:/dev/block/bootdevice/by-name/boot:12793108:b5f41a01217bc3b1b3521d2c0f0e24ad577521bb EMMC:/dev/block/bootdevice/by-name/recovery 23fd70a6b1bf0ac1fc509b3e2ddf6098bea2fa92 35833112 b5f41a01217bc3b1b3521d2c0f0e24ad577521bb:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
